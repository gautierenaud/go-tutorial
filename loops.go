package main

func forLoop() int {
    sum := 0
    for i := 0; i < 10; i++ {
        sum += i
    }
    return sum
}

func forLoopSmaller() int {
    sum := 0
    // init and post statements are optional
    for ; sum < 45; {
        sum += sum
    }
    return sum
}

func forLoopWhile() int {
    sum := 0
    // init and post statements are optional
    for sum < 45 {
        sum += sum
    }
    return sum
}

func infiniteLoop() {
    for {
    }
}

// Sqrt compute the square root of x and returns it
func Sqrt(x float64) float64 {
    z := 1.
    for i := 0; i < 10; i++ {
        z -= (z * z - x) / (2 * z)
    }
    return z
}
