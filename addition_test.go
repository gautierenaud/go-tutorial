package main

import "testing"

func TestAdd(t *testing.T) {
    total := Add(1, 2)
    if total != 3 {
        t.Errorf("Incorrect sum : %d instead of %d", total, 3)
    }
}
