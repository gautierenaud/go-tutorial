package main

// Pi is an exported const that should have comments
const Pi = 3.14
const dummy = 0

// Add will add two integers and return the result.
func Add(x, y int) int {
    return add(x, y)
}

func add(x, y int) (z int) {
    z = x + y
    return
}
