package main

import (
    "fmt"
    "math"
)

func pow(x, n, lim float64) float64 {
    // we can also add statements before the actual condition
    if v := math.Pow(x, n); v < lim {
        return v
    } else if x == 2 {
        // we can access the variable declared in the condition above
        // from other 'else' blocks
        fmt.Printf("%g >= %g\n", v, lim)
    }
    return lim
}
