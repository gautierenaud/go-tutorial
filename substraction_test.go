package main

import "testing"

func TestSub(t *testing.T) {
    substract := Sub(3, 2)
    if (substract != 1) {
        t.Errorf("Incorrect substraction : %d instead of %d", substract, 1)
    }
}
