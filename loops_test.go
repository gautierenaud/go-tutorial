package main

import "testing"

func TestSqrt(t *testing.T) {
    sqrt := Sqrt(64.)
    if sqrt != 8 {
        t.Errorf("Incorrect root : %f instead of %f", sqrt, 8.)
    }
}
